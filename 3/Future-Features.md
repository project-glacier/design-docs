# Future Features
- Blur/Gaussian Blur
- Keyboard Shortcuts
- Keyboard Navigation
- Prototyping workflow
- Plugins (Could use Python for the scripting language and manually look through the code for security)
- Pen Tool - Create shapes from vectors.