# design-docs

Design Documents for Project Glacier. It's a top-down view of the whole project rather than focusing on specific details.

## Overview


## Table of contents
[1. Aims and Goals](1/Aims-and-Goals.md)

[2. Planned Features](2/Planned-Features.md)

[3. Future Features](3/Future-Features.md)

[4. UI Mockups](4/UI-Mockups.md)

[5. Additional Notes](5/Additional-Notes.md)


