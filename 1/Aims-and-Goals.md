# Aims and Goals

## Goal
A tool to allow people to design and prototype interaction interfaces.

## Pillars
- Native
- High Performance
- Adaptive
- Open-Source

## Target Platform
Linux (Mainly supporting Ubuntu but be as adaptive as possible for other distros)

## Purpose
- Not everyone has an always-on internet connection (Figma is not for everyone)
- Allow people to do design offline on Linux

## Pricing and availability
- Free for everyone! 