# Planned Features
## Feature List
- Shape objects
- Areas to place collections of objects inside
- Screens/Views
- Desks - Collection of areas of collection. (Something like pages in Figma)
- Corner Rounding (Radius)
- Text Tool
- Shadows
- Styles
- Components
- Groups
- Design workflow ONLY. Protoyping will happen later

## Implementation

### Canvas Objects
- Shape object features: 
    - Fill (colour)
    - Outline (colour) that has adjustable thickness
    - Corner radius values (Some shapes):
      - One value for all corners
      - Or 4 values corresponding to each corner
- Text tool features:
  - Write text
  - Adjust foreground colour
  - Adjust text alignment (Left, centre, right, maybe others...)
  - Adjust Font Size
  - Adjust Font Weight
- Image Support (For V1, will used the easiest support as possible)
- Colour brush types:
  - Plain Colours
  - Images? (Not guaranteed for V1)

- ### Desk Objects
- Groups (A collection of canvas objects. )